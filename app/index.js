'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');


var EtchPreviewGenerator = module.exports = function EtchPreviewGenerator(args, options, config) {
    yeoman.generators.Base.apply(this, arguments);

    this.on('end', function () {
        this.installDependencies({ skipInstall: options['skip-install'] });
    });

    this.pkg = JSON.parse(this.readFileAsString(path.join(__dirname, '../package.json')));
};

util.inherits(EtchPreviewGenerator, yeoman.generators.Base);

EtchPreviewGenerator.prototype.askFor = function askFor() {
  var cb = this.async();

  // have Yeoman greet the user.
  console.log(this.yeoman);

  var prompts = [{
    type: 'value',
    name: 'projectName',
    message: 'What would you name this project?',
    default: 'Etch'
  }];

  this.prompt(prompts, function (answers) {
    this.projectName = answers.projectName;

    cb();
  }.bind(this));
};

EtchPreviewGenerator.prototype.app = function app() {
    this.mkdir('app');
    this.mkdir('app/images');
    this.mkdir('app/scripts');
    this.mkdir('app/styles');
    this.mkdir('app/templates');
    this.mkdir('app/templates/layouts');
    this.mkdir('app/templates/includes');

    this.template('Gruntfile.js');

    this.copy('_bower.json', 'bower.json');
    this.copy('_package.json', 'package.json');
};

EtchPreviewGenerator.prototype.dist = function app() {
    this.mkdir('dist');
};

EtchPreviewGenerator.prototype.projectFiles = function projectFiles() {
    this.copy('bowerrc', '.bowerrc');
    this.copy('editorconfig', '.editorconfig');
    this.copy('gitignore', '.gitignore');
    this.copy('jshintrc', '.jshintrc');
};

EtchPreviewGenerator.prototype.templateFiles = function templateFiles() {
    this.copy('default_layout.jade', 'app/templates/layouts/default.jade');
    this.copy('index.jade', 'app/templates/index.jade');
    this.write('app/templates/includes/.gitkeep', '');
};

EtchPreviewGenerator.prototype.styleFiles = function styleFiles() {
    this.copy('main.less', 'app/styles/main.less');
};

EtchPreviewGenerator.prototype.scriptFiles = function scriptFiles() {
    this.write('app/scripts/.gitkeep', '');
};

EtchPreviewGenerator.prototype.imageFiles = function imageFiles() {
    this.write('app/images/.gitkeep', '');
};
